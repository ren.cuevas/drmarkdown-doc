package com.drmarkdown.doc.services;

import java.util.List;

/**
 * This file was created by aantonica on 20/05/2020
 */
public interface TokenService {

    String getUserId(String jwtToken);

    List<String> getUserRoles(String jwtToken);
}
