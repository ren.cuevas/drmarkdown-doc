package com.drmarkdown.doc.daos;

import com.drmarkdown.doc.models.DocModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * This file was created by aantonica on 20/05/2020
 */
@Repository
public interface DocDAO extends MongoRepository<DocModel, String> {

    List<DocModel> findAllByUserIdOrderByUpdatedAtDesc(String userID);

    Page<DocModel> findByAvailable(boolean b, PageRequest updatedAt);
    
}
